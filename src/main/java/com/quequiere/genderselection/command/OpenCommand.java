package com.quequiere.genderselection.command;

import java.util.ArrayList;
import java.util.List;

import com.quequiere.genderselection.network.OpenMainMenuPacket;
import com.quequiere.genderselection.network.PacketService;

import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class OpenCommand implements ICommand {

	@Override
	public int compareTo(Object paramT) {
		return 0;
	}

	@Override
	public String getCommandName() {
		return "genderselection";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_) {
		return "genderselection";
	}

	@Override
	public List getCommandAliases() {
		return null;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) {
		if (sender instanceof EntityPlayer) {
			if (args.length == 0) {
				PacketService.network.sendTo(new OpenMainMenuPacket(), (EntityPlayerMP) sender);
			}

		}

	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_) {
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_) {
		return false;
	}

	public static boolean doesStringStartWith(String p_71523_0_, String p_71523_1_) {
		return p_71523_1_.regionMatches(true, 0, p_71523_0_, 0, p_71523_0_.length());
	}

}
