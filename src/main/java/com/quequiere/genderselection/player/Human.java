package com.quequiere.genderselection.player;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.genderselection.network.AskSkin;
import com.quequiere.genderselection.network.PacketService;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class Human {

	public static HashMap<String, Human> clientsLoaded = new HashMap<String, Human>();
	public static HashMap<String, Human> serverHumanLoaded = new HashMap<String, Human>();
	
	
	public static HashMap<String, ResourceLocation> clientLoadedSkins = new HashMap<String, ResourceLocation>();

	private transient AbstractClientCommon player;
	private Gender gender;
	private String skinName;
	private String playerName;

	public Human(Gender gender, String skinName,String playerName) {
		this.skinName = skinName;
		this.gender = gender;
		this.playerName=playerName;
	}

	public Gender getGender() {
		return gender;
	}

	public void setPlayer(AbstractClientCommon player) {
		this.player = player;
	}

	// client Side
	public ResourceLocation getSkin() {
		ResourceLocation loc = clientLoadedSkins.get(this.skinName);

		if (loc == null) {
			PacketService.network.sendToServer(new AskSkin(this.skinName,this.getGender()));
			System.out.println("Try to downloadSkin from server ...");
		}

		return clientLoadedSkins.get(this.getSkinName());
	}

	// client Side
	public void updateSkin()
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		
		this.player.updateSkin(this);
	}
	
	public String toJson()
	{
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public static Human fromJson(String s)
	{
		Gson gson = new Gson();
		return gson.fromJson(s, Human.class);
	}

	public String getPlayerName() {
		return playerName;
	}
	
	public boolean needUpdate()
	{
		if(this.player==null)
		{
			return true;
		}
		return false;
	}

	public String getSkinName() {
		return skinName;
	}
	
	
}
