package com.quequiere.genderselection.player;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.util.ResourceLocation;

public class AbstractClientCommon {
	
	public AbstractClientPlayer player;
	
	public AbstractClientCommon(AbstractClientPlayer p)
	{
		player=p;
	}

	public void updateSkin(Human h) throws IllegalArgumentException, IllegalAccessException
	{

		for (Field f : AbstractClientPlayer.class.getDeclaredFields()) {
			f.setAccessible(true);
			boolean m = Modifier.isFinal(f.getModifiers());
			Object o = f.get(this.player);
			if (f.getType().equals(ResourceLocation.class) && !m) {
				f.set(this.player, h.getSkin());
				System.out.println("loading !!!");
				break;
			}
		}
	
	}
}
