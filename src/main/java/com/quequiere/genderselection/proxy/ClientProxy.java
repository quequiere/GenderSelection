package com.quequiere.genderselection.proxy;

import com.quequiere.genderselection.client.render.RenderEvent;
import com.quequiere.genderselection.server.event.PlayerEvent;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy  extends CommonProxy 
{
	public void RegisterRender()
	{
		MinecraftForge.EVENT_BUS.register(new RenderEvent());
		FMLCommonHandler.instance().bus().register(new RenderEvent());
	}
}
