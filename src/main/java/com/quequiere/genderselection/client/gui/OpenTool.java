package com.quequiere.genderselection.client.gui;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;

public class OpenTool
{
	public static void OpenDistance(Class<? extends GuiScreen> c)
	{
		Minecraft mc = Minecraft.getMinecraft();

		if (FMLClientHandler.instance().getClient().currentScreen == null)
		{
		
			
			try
			{
				Object o = c.newInstance();
				mc.displayGuiScreen((GuiScreen) o);
			}
			catch (InstantiationException e)
			{
				e.printStackTrace();
			}
			catch (IllegalAccessException e)
			{
				e.printStackTrace();
			}
			
				
			
		}

	}
}
