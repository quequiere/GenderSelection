package com.quequiere.genderselection.client.gui;

import java.awt.Button;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.util.Color;

import com.quequiere.genderselection.GenderSelection;
import com.quequiere.genderselection.network.ChangeSkin;
import com.quequiere.genderselection.network.PacketService;
import com.quequiere.genderselection.player.Gender;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.registry.GameData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.RegistryNamespaced;
import net.minecraft.util.ResourceLocation;

public class SelectionGui extends GuiScreen {

	private TextureManager textureManager;

	private HashMap<String, ResourceLocation> textures = new HashMap<String, ResourceLocation>();
	private GuiButton gender, prev, next, valider;
	private int current = 0;
	private Gender gen = Gender.male;

	public SelectionGui() {
		super();
	}

	public void initGui() {
		gender = new GuiButton(0, this.width / 2 - 35, 20, 70, 20, "Homme");
		prev = new GuiButton(1, 25, this.height / 2, 50, 20, "<");
		next = new GuiButton(2, this.width - 75, this.height / 2, 50, 20, ">");
		valider = new GuiButton(0, this.width / 2 - 35, this.height - 40, 70, 20, "Valider");

		this.buttonList.add(gender);
		this.buttonList.add(prev);
		this.buttonList.add(next);
		this.buttonList.add(valider);

		PacketService.network.sendToServer(
				new ChangeSkin((String) GenderSelection.skinMaleL.get(current), Gender.male));
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);

		String name = "";

		if (gen.equals(Gender.male)) {
			name = (String) GenderSelection.skinMaleL.get(current);

			int size = GenderSelection.skinMaleL.size();
			if (current == 0) {
				prev.enabled = false;
			} else {
				prev.enabled = true;
			}

			if (current + 1 == size) {
				next.enabled = false;
			} else {
				next.enabled = true;
			}

		} else if (gen.equals(Gender.female)) {
			name = (String) GenderSelection.skinFemaleL.get(current);
			int size = GenderSelection.skinFemaleL.size();
			if (current == 0) {
				prev.enabled = false;
			} else {
				prev.enabled = true;
			}

			if (current + 1 == size) {
				next.enabled = false;
			} else {
				next.enabled = true;
			}
		}

		this.drawCenteredString(this.fontRendererObj, name, this.width / 2, 50, 0xffffffff);
	}

	@Override
	protected void actionPerformed(final GuiButton button) {

		if (button.equals(gender)) {
			if (gender.displayString.equals("Homme")) {
				gender.displayString = "Femme";
				gen = Gender.female;
				current = 0;
			} else {
				gender.displayString = "Homme";
				gen = Gender.male;
				current = 0;
			}
			
			if (gen.equals(Gender.male)) {
				PacketService.network.sendToServer(
						new ChangeSkin((String) GenderSelection.skinMaleL.get(current), Gender.male));
			} else if (gen.equals(Gender.female)) {
				PacketService.network.sendToServer(new ChangeSkin(
						(String) GenderSelection.skinFemaleL.get(current), Gender.female));
			}
		} else if (button.equals(valider)) {
			Minecraft.getMinecraft().currentScreen=null;
			
		} else if (button.equals(prev)) {
			current--;

			if (gen.equals(Gender.male)) {
				PacketService.network.sendToServer(
						new ChangeSkin((String) GenderSelection.skinMaleL.get(current), Gender.male));
			} else if (gen.equals(Gender.female)) {
				PacketService.network.sendToServer(new ChangeSkin(
						(String) GenderSelection.skinFemaleL.get(current), Gender.female));
			}

		} else if (button.equals(next)) {
			current++;
			if (gen.equals(Gender.male)) {
				PacketService.network.sendToServer(
						new ChangeSkin((String) GenderSelection.skinMaleL.get(current), Gender.male));
			} else if (gen.equals(Gender.female)) {
				PacketService.network.sendToServer(new ChangeSkin(
						(String) GenderSelection.skinFemaleL.get(current), Gender.female));
			}
		}

		super.actionPerformed(button);
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}

}
