package com.quequiere.genderselection.client.gui;

import net.minecraft.client.renderer.Tessellator;

public class ImgTools {

	 public static void myDrawTexturedModalRect(double x, double y, double width, double height)
	 {
	     Tessellator tessellator = Tessellator.instance;
	     tessellator.startDrawingQuads();    
	     tessellator.addVertexWithUV(x        , y + height, 0, 0.0, 1.0);
	     tessellator.addVertexWithUV(x + width, y + height, 0, 1.0, 1.0);
	     tessellator.addVertexWithUV(x + width, y         , 0, 1.0, 0.0);
	     tessellator.addVertexWithUV(x        , y         , 0, 0.0, 0.0);
	     tessellator.draw();
	 }
}
