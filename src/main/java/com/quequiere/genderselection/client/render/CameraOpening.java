package com.quequiere.genderselection.client.render;

import org.lwjgl.opengl.GL11;

public class CameraOpening {

	public int timer = 0;
	public double verticale, vspeed = 0;
	public double hauteur, haspeed = 0;
	public double horizontale, hospeed = 0;

	public boolean ended = false;

	public CameraOpening() {
		this.verticale = -4.47d;
		// this.verticale=-20;

		this.hauteur = -7;
		this.haspeed = 0.01;

	}

	public void update() {

		double verpos = -1 * Math.pow(this.verticale, 2)+30;
		
		
		if(verpos<=0 && this.verticale>0)
		{
			
		}
		else
		{
			this.verticale+=0.014;
		}


		if (this.hauteur > 0) {
			this.hauteur = 0;
		} else if (this.hauteur < 0) {
			this.hauteur += haspeed;
		}

		 GL11.glRotated(verpos, 0, 0, 1);
		 GL11.glTranslated(0, this.hauteur, 0);
	}

}
