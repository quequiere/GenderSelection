package com.quequiere.genderselection.client.render;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.opengl.GL11;

import com.quequiere.genderselection.client.gui.SelectionGui;
import com.quequiere.genderselection.network.AskHuman;
import com.quequiere.genderselection.network.ChangeSkin;
import com.quequiere.genderselection.network.PacketService;
import com.quequiere.genderselection.player.AbstractClientCommon;
import com.quequiere.genderselection.player.Gender;
import com.quequiere.genderselection.player.Human;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.client.event.GuiScreenEvent.DrawScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.client.event.RenderWorldEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.client.event.EntityViewRenderEvent.RenderFogEvent;

public class RenderEvent {

	public static HashMap<String, Long> cached = new HashMap<String, Long>();

	private double playerRotation = 0;
	private CameraOpening cam;

	@SubscribeEvent
	public void renduDeLobjet(RenderPlayerEvent.Pre event) {

		//
	}

	@SubscribeEvent
	public void renduDuScreen(RenderFogEvent event) {

		if (Minecraft.getMinecraft().currentScreen != null
				&& Minecraft.getMinecraft().currentScreen.getClass().equals(SelectionGui.class)) {
			
			if(this.cam==null)
			{
				this.cam=new CameraOpening();
			}
			
			
			if(this.cam.ended)
			{
				if (playerRotation > 360) {
					playerRotation = 0;
				}
				playerRotation += 0.06f;
				GL11.glRotated(playerRotation, 0, 1, 0);
			}
			else
			{
				this.cam.update();
			}

		}
		else if(this.cam!=null)
		{
			this.cam=null;
		}

	}

	@SubscribeEvent
	public void RenderSkin(RenderPlayerEvent.Post event) {
		AbstractClientPlayer acp = (AbstractClientPlayer) event.entity;
		Human h = Human.clientsLoaded.get(acp.getDisplayName());

		if (h == null && cached.get(acp.getDisplayName()) == null) {

			PacketService.network.sendToServer(new AskHuman(acp.getDisplayName()));
			cached.put(acp.getDisplayName(), System.currentTimeMillis() / 1000);

		} else if (h != null && h.needUpdate()) {
			h.setPlayer(new AbstractClientCommon(acp));
			try {
				h.updateSkin();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		} else {

			if (cached.get(acp.getDisplayName()) != null) {

				long last = cached.get(acp.getDisplayName());
				long diff = System.currentTimeMillis() / 1000 - last;
				if (diff > 5) {
					cached.remove(acp.getDisplayName());
				}
			}

		}
	}

}
