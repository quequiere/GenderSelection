package com.quequiere.genderselection.network;

import com.quequiere.genderselection.client.render.RenderEvent;
import com.quequiere.genderselection.player.Human;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;

public class SendHuman implements IMessage {

	private String compress=null;
	private boolean complet = true;
	
	public SendHuman() {
		
	}

	public SendHuman(boolean b) {
		this.complet=false;
	}

	public SendHuman(Human h) {
		this.compress = h.toJson();
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		
		this.complet=buf.readBoolean();
		
		if(this.complet)
		{
			this.compress = ByteBufUtils.readUTF8String(buf);
		}
		
	}

	@Override
	public void toBytes(ByteBuf buf) {
		
		buf.writeBoolean(complet);
		
		if(complet)
		{
			ByteBufUtils.writeUTF8String(buf, this.compress);
		}
	}

	public static class Handler implements IMessageHandler<SendHuman, IMessage> {

		@Override
		public IMessage onMessage(SendHuman message, MessageContext ctx) {

			if(message.complet)
			{
				Human h = Human.fromJson(message.compress);
				Human.clientsLoaded.put(h.getPlayerName(), h);
				RenderEvent.cached.remove(h.getPlayerName());
			}
			else
			{
				System.out.println("cant load a skin");
			}

			return null;
		}
	}
}
