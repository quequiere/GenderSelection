package com.quequiere.genderselection.network;

import java.io.File;

import com.quequiere.genderselection.player.Gender;
import com.quequiere.genderselection.player.Human;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;

public class AskSkin implements IMessage {

	private String name;
	private Gender gender;

	public AskSkin() {

	}

	public AskSkin(String skinName,Gender gender) {
		this.name = skinName;
		this.gender=gender;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.name = ByteBufUtils.readUTF8String(buf);
		this.gender = Gender.valueOf(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.name);
		ByteBufUtils.writeUTF8String(buf, this.gender.name());
	}

	public static class Handler implements IMessageHandler<AskSkin, IMessage> {

		@Override
		public IMessage onMessage(AskSkin message, MessageContext ctx) {

			String folderpath = "./config/genderselection/"+message.gender.name()+"/"+message.name+".png";
			File img = new File(folderpath);
			return new SendSkin(message.name,img);
		}
	}
}
