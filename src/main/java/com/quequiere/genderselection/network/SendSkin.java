package com.quequiere.genderselection.network;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.quequiere.genderselection.client.render.RenderEvent;
import com.quequiere.genderselection.player.Human;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;

public class SendSkin implements IMessage {

	private String name;
	private byte[] compressedImg;

	public SendSkin() {

	}

	public SendSkin(String name, File img) {
		this.name = name;

		compressedImg = new byte[(int) img.length()];

		try {
			FileInputStream fileInputStream = new FileInputStream(img);
			fileInputStream.read(compressedImg);
			fileInputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.name = ByteBufUtils.readUTF8String(buf);
		byte[] bytes = new byte[buf.readableBytes()];
		buf.readBytes(bytes);
		compressedImg = bytes;
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.name);
		buf.writeBytes(compressedImg);
	}

	public static class Handler implements IMessageHandler<SendSkin, IMessage> {

		@Override
		public IMessage onMessage(SendSkin message, MessageContext ctx) {

			ByteArrayInputStream bais = new ByteArrayInputStream(message.compressedImg);

			try {

				BufferedImage image = ImageIO.read(bais);

				DynamicTexture dt = new DynamicTexture(image);
				TextureManager textureManager = FMLClientHandler.instance().getClient().renderEngine;
				ResourceLocation loc = textureManager.getDynamicTextureLocation(message.name, dt);

				Human.clientLoadedSkins.put(message.name, loc);

				for (Human h : Human.clientsLoaded.values()) {
					if (h.getSkinName().equals(message.name)) {
						try {
							h.updateSkin();
						} catch (NoSuchFieldException e) {
							e.printStackTrace();
						} catch (SecurityException e) {
							e.printStackTrace();
						} catch (IllegalArgumentException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}
	}
}
