package com.quequiere.genderselection.network;

import java.io.File;

import com.quequiere.genderselection.GenderSelection;
import com.quequiere.genderselection.player.Gender;
import com.quequiere.genderselection.player.Human;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.util.ResourceLocation;

public class ChangeSkin implements IMessage {

	private String name;
	private Gender gender;

	public ChangeSkin() {

	}

	public ChangeSkin(String skinName, Gender gender) {

		this.name = skinName;
		this.gender = gender;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.name = ByteBufUtils.readUTF8String(buf);
		this.gender = Gender.valueOf(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.name);
		ByteBufUtils.writeUTF8String(buf, this.gender.name());
	}

	public static class Handler implements IMessageHandler<ChangeSkin, IMessage> {

		@Override
		public IMessage onMessage(ChangeSkin message, MessageContext ctx) {

			String pname = ctx.getServerHandler().playerEntity.getDisplayName();

			File img = GenderSelection.getSkinFile(message.name, message.gender);
			if (img == null) {
				System.out.println("fatal error");
				return null;
			}
			if (!img.exists()) {
				System.out.println("Can find skin: " + message.name);
				return null;
			}

			Human h = new Human(message.gender, message.name, pname);
			Human.serverHumanLoaded.remove(pname);
			Human.serverHumanLoaded.put(pname, h);
			return new SendHuman(h);
		}
	}
}
