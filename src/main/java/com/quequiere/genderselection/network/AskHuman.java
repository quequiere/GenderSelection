package com.quequiere.genderselection.network;

import com.quequiere.genderselection.player.Human;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;

public class AskHuman implements IMessage {

	private String name;

	public AskHuman() {

	}

	public AskHuman(String playerName) {
		this.name = playerName;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.name = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.name);
	}

	public static class Handler implements IMessageHandler<AskHuman, IMessage> {

		@Override
		public IMessage onMessage(AskHuman message, MessageContext ctx) {

			Human h = Human.serverHumanLoaded.get(message.name);
			if(h==null)
			{
				return new SendHuman(false);
			}
			return new SendHuman(h);
		}
	}
}
