package com.quequiere.genderselection.network;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class PacketService {

	public static SimpleNetworkWrapper network;
	private static int x = -1;

	public static void init() {

		PacketService.network = NetworkRegistry.INSTANCE.newSimpleChannel("GenderSelection");

		PacketService.network.registerMessage(AskHuman.Handler.class, AskHuman.class, x++, Side.SERVER);
		PacketService.network.registerMessage(ChangeSkin.Handler.class, ChangeSkin.class, x++, Side.SERVER);
		PacketService.network.registerMessage(AskSkin.Handler.class, AskSkin.class, x++, Side.SERVER);
		
		
		PacketService.network.registerMessage(SendHuman.Handler.class, SendHuman.class, x++, Side.CLIENT);
		PacketService.network.registerMessage(SendSkin.Handler.class, SendSkin.class, x++, Side.CLIENT);
		PacketService.network.registerMessage(OpenMainMenuPacket.Handler.class, OpenMainMenuPacket.class, x++, Side.CLIENT);
		
		
		
		
		
	
		
		
	}

}
