package com.quequiere.genderselection.network;


import com.quequiere.genderselection.GenderSelection;
import com.quequiere.genderselection.client.gui.SelectionGui;
import com.quequiere.genderselection.client.gui.OpenTool;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class OpenMainMenuPacket implements IMessage
{

	
	
	public OpenMainMenuPacket()
	{
		
	}
	


	@Override
	public void fromBytes(ByteBuf buf)
	{
		String male = ByteBufUtils.readUTF8String(buf);
		String female = ByteBufUtils.readUTF8String(buf);
		
		GenderSelection.skinMaleL.clear();
		GenderSelection.skinFemaleL.clear();
		
		for(String s:male.split("#"))
		{
			GenderSelection.skinMaleL.add(s);
		}
		
		for(String s:female.split("#"))
		{
			GenderSelection.skinFemaleL.add(s);
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		String male = "";
		for(String s:GenderSelection.skinListMale.keySet())
		{
			if(male!="")
			{
				male+="#"+s;
			}
			else
			{
				male=s;
			}
		}
		
		ByteBufUtils.writeUTF8String(buf, male);
		
		String female = "";
		for(String s:GenderSelection.skinListFemale.keySet())
		{
			if(female!="")
			{
				female+="#"+s;
			}
			else
			{
				female=s;
			}
		}
		
		ByteBufUtils.writeUTF8String(buf, female);
	}

	public static class Handler implements IMessageHandler<OpenMainMenuPacket, IMessage>
	{

		@Override
		public IMessage onMessage(OpenMainMenuPacket m, MessageContext ctx)
		{
			OpenTool.OpenDistance(SelectionGui.class);
			Minecraft.getMinecraft().gameSettings.thirdPersonView=1;
			return null; 
		}
	}

}
