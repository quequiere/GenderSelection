package com.quequiere.genderselection;

import net.minecraft.init.Blocks;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.genderselection.command.OpenCommand;
import com.quequiere.genderselection.network.PacketService;
import com.quequiere.genderselection.player.Gender;
import com.quequiere.genderselection.proxy.CommonProxy;
import com.quequiere.genderselection.server.event.PlayerEvent;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.relauncher.Side;

@Mod(modid = GenderSelection.MODID, version = GenderSelection.VERSION)
public class GenderSelection {
	public static final String MODID = "GenderSelection";
	public static final String VERSION = "1.0";

	@SidedProxy(clientSide = "com.quequiere.genderselection.proxy.ClientProxy", serverSide = "com.quequiere.genderselection.proxy.CommonProxy")
	public static CommonProxy proxy;

	public static HashMap<String, File> skinListMale = new HashMap<String, File>();
	public static HashMap<String, File> skinListFemale = new HashMap<String, File>();
	
	public static ArrayList<String> skinMaleL = new  ArrayList<String>();
	public static ArrayList<String> skinFemaleL = new  ArrayList<String>();

	@EventHandler
	public void init(FMLInitializationEvent event) {
		PacketService.init();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		proxy.RegisterRender();
	}

	@EventHandler
	public void startedServer(FMLServerStartedEvent event) {
		FMLCommonHandler.instance().bus().register(new PlayerEvent());

		for (Gender g : Gender.values()) {
			String folderpath = "./config/genderselection/" + g;
			File folder = new File(folderpath);
			if (!folder.exists()) {
				folder.mkdirs();
			}
		}

	}

	@EventHandler
	public void serverLoad(FMLServerStartingEvent event) {

		event.registerServerCommand(new OpenCommand());
		reloadSkinFolder();
	}

	public static void reloadSkinFolder() {
		for (Gender g : Gender.values()) {
			String folderpath = "./config/genderselection/" + g;
			File folder = new File(folderpath);

			for (File skin : folder.listFiles()) {
				String name = skin.getName().replace(".png", "");
				System.out.println("Skin loading: "+name);
				if (g.equals(Gender.female)) {

					skinListFemale.put(name, skin);
				} else if (g.equals(Gender.male)) {
					skinListMale.put(name, skin);
				}
			}

		}
	}

	public static File getSkinFile(String name, Gender g) {
		if (g.equals(Gender.female)) {
			return skinListFemale.get(name);
		} else if (g.equals(Gender.male)) {
			return skinListMale.get(name);
		}

		return null;
	}

}
